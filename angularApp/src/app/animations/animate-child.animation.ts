import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, query, animateChild, keyframes } from '@angular/animations';

export const AnimateChildAnimation = [
  trigger('slideInParentAnimation',[
    transition(':enter', [
      query(".sliding-element", animate('900ms', keyframes([
        style({ transform: 'translateX(-200px)', offset: 0 }),  
        style({ transform: 'translateX(0)', offset: 1 })  
      ]))),
        query('@slideInChildAnimation', animateChild())
    ])
  ]),
  trigger('slideInChildAnimation',[
    transition(':enter', [
      query(".sliding-child-element", animate('2000ms', keyframes([
        style({ opacity: 0, offset: 0 }),  
        style({ opacity: 1, offset: 1, 'font-size': '26px' })  
      ]))),
    ])
  ])
];