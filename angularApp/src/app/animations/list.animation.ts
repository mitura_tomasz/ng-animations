import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, query, stagger } from '@angular/animations';

export const ListAnimation = trigger('listAnimation',
  [
    transition('* => *', [
      query('img', style({ transform: 'translateX(-100%)' })),
      query('img', 
        stagger('600ms', [
          animate('900ms', style({ transform: 'translateX(0)' }))
      ]))
    ])
  ]
);
