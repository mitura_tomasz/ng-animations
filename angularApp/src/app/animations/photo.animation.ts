import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

export const PhotoAnimation = trigger('photoAnimation',
  [
    state('move', style({
      transform: 'translateX(100px) translateY(50px)'
    })),
    state('enlarge', style({
      transform: 'scale(1.5)'
    })),
    state('spin', style({
      transform: 'rotateY(180deg) rotateZ(90deg)'
    })),
    transition('* => *', animate('2000ms ease-out'))
  ]
);
