import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

export const MoveWithParamsAnimation = trigger('moveWithParamsAnimation',[
  state('move', style({
    transform: 'translateX({{ moveDistance }}px)'
  }),
  { params: { moveDistance: '0' } }
  ),
  transition('* => *', animate('2000ms ease-out'), { params: { moveDistance: "0" } })
]);
