import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, sequence } from '@angular/animations';

export const SequenceAnimation = trigger('sequenceAnimation',
  [
    transition(':enter', [
      style({ // własciwosci aktywne na poczatku animacji
        backgroundColor: 'red',
        opacity: 0,
        transform: 'translateX(0)'
      }),
      sequence([
        animate('2s 0.1s ease', style({ // animacja rozlozona w czanie na okres 2 sekund
          opacity: 1
        })),
        animate('1s 0.1s ease', style({ // animacja rozlozona w czanie na okres x sekund
          'transform': 'translateX(200px)'
        })),
        animate('1s 0.1s ease-in', style({ // animacja rozlozona w czanie na okres x sekundy
          backgroundColor: '#3a6c95',
        })),
        animate('1s 0.1s ease', style({ // animacja rozlozona w czanie na okres x sekund
          transform: 'translateX(0)'
        }))
      ])
    ])
  ]
);
