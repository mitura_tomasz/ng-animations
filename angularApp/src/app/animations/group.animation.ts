import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, group } from '@angular/animations';

export const GroupAnimation = trigger('groupAnimation',
  [
    transition(':enter', [
      style({ // własciwosci aktywne na poczatku animacji
        backgroundColor: 'red',
        'font-size': '28px'
      }),
      group([
        animate('1s 0.1s ease-in', style({ // animacja rozlozona w czanie na okres 1 sekundy
          backgroundColor: '#3a6c95',
        })),
        animate('3s 0.1s ease', style({ // animacja rozlozona w czanie na okres 3 sekund
          'font-size': '16px'
        }))
      ])
    ])
  ]
);