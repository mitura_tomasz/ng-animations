import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

export const JumpAnimation = trigger('jumpAnimation',
  [
    state('jump', style({
      transform: 'translateX(500px) translateY(0px)'
    })),
    state('return', style({
      transform: 'translateX(0px) translateY(0px)'
    })),
    transition('return => jump', 
      animate('2000ms', keyframes([
        style({transform: 'translateX(0) translateY(0%)', offset: 0}),
        style({transform: 'translateX(100px) translateY(30%)', offset: 0.33}),
        style({transform: 'translateX(250px) translateY(5%)', offset: 0.66}),
        style({transform: 'translateX(300px) translateY(51%)', offset: 0.88}),
        style({transform: 'translateX(500px) translateY(0)', offset: 1})
      ])
    )),
    transition('jump => return', 
      animate('2000ms', keyframes([
        style({transform: 'translateX(500px) translateY(0)', offset: 0}),
        style({transform: 'translateX(300px) translateY(51%)', offset: 0.33}),
        style({transform: 'translateX(250px) translateY(5%)', offset: 0.66}),
        style({transform: 'translateX(100px) translateY(30%)', offset: 0.88}),
        style({transform: 'translateX(0) translateY(0%)', offset: 1})
      ])
    )),
  ]
);
