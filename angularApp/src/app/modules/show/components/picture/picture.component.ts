import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

import { PopOverAnimation } from './../../../../animations/pop-over.animation';
import { PhotoAnimation } from './../../../../animations/photo.animation';
import { JumpAnimation } from './../../../../animations/jump.animation';
import { PopOverCallbacksAnimation } from './../../../../animations/pop-over-callbacks.animation';
import { ListAnimation } from './../../../../animations/list.animation';
import { AnimateChildAnimation } from './../../../../animations/animate-child.animation';
import { GroupAnimation } from './../../../../animations/group.animation';
import { SequenceAnimation } from './../../../../animations/sequence.animation';
import { MoveWithParamsAnimation } from './../../../../animations/moveWithParams.animation';

@Component({
  selector: 'app-picture',
  templateUrl: './picture.component.html',
  styleUrls: ['./picture.component.css'],
  animations: [ 
    PopOverAnimation,
    PhotoAnimation,
    JumpAnimation,
    PopOverCallbacksAnimation,
    ListAnimation,
    AnimateChildAnimation,
    GroupAnimation,
    SequenceAnimation,
    MoveWithParamsAnimation
  ],
})
export class PictureComponent implements OnInit {

  popOverState: boolean = false;
  photoStateMove: boolean = false;
  photoStateEnlarge: boolean = false;
  photoStateSpin: boolean = false;
  photoStateSelectedAnimation: string;
  jumpState: boolean = false;
  popOverCallbacksState: boolean = false;
  listState: boolean = false;
  photos: string[] = [
    'http://oi41.tinypic.com/negqoo.jpg',
    'http://oi41.tinypic.com/2vude3a.jpg',
    'http://oi42.tinypic.com/2sbmuyg.jpg'
  ];
  moveWithParamsState: boolean = false;
  
  constructor() { }

  ngOnInit() { }
  
  // popOver-animation
  getPopOverState() {
    return this.popOverState ? 'hide' : 'show'
  }
  togglePopOver() {
    this.popOverState = !this.popOverState;
  }
  
  // photo-animation
  getPhotoState() {
    switch (this.photoStateSelectedAnimation) {
      case "move": return this.getPhotoMoveState();
      case "enlarge": return this.getPhotoEnlargeState();
      case "spin": return this.getPhotoSpinState();
      default: return null;
    }
  }
  
  getPhotoMoveState() {
      return this.photoStateMove ? 'move': null    
  }
  getPhotoEnlargeState() {
    return this.photoStateEnlarge ? 'enlarge': null    
  }
  getPhotoSpinState() {
    return this.photoStateSpin ? 'spin': null    
  }
  
  toggleMove() {
    this.photoStateSelectedAnimation = "move";
    this.photoStateMove = !this.photoStateMove;
  }
  toggleEnlarge() {
    this.photoStateSelectedAnimation = "enlarge";
    this.photoStateEnlarge = !this.photoStateEnlarge;
  }
  toggleSpin() {
    this.photoStateSelectedAnimation = "spin";
    this.photoStateSpin = !this.photoStateSpin;
  }
  
  // jump-animation
  getjumpState() {
    return this.jumpState ? 'jump': 'return'    
  }
  toggleJump() {
    this.jumpState = !this.jumpState;
  }
  
  
  // popOverCallbacks-animation
  togglePopOverCallbacks() {
    this.popOverCallbacksState = !this.popOverCallbacksState;
  }
  getPopOverCallbacksState() {
    return this.popOverCallbacksState ? 'hide' : 'show'
  }
  popOverOnStartMessage(event) {
    console.log('\nstart animation');
    console.log('event.fromState', event.fromState);
    console.log('event.toState', event.toState);
    console.log('event.totalTime', event.totalTime, '\n\n');
  }
  popOverOnEndMessage(event) {
    console.log('\ndone animation');
    console.log('event.fromState', event.fromState);
    console.log('event.toState', event.toState);
    console.log('event.totalTime', event.totalTime, '\n\n');    
  }
  
  // list
  toggleList() {
    this.listState = !this.listState;
  }
  
  // moveWithParamsAnimation
  getMoveWithParamsState(): string | null {
    return this.moveWithParamsState ? 'move': null;
  }
  
  switchMoveWithParamsState(): void {
    this.moveWithParamsState = !this.moveWithParamsState;
  }
}
