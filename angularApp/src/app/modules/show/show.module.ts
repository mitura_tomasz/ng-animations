import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//import { ShowRoutingModule } from './show-routing.module';
import { ShowComponent } from './show.component';
import { PictureComponent } from './components/picture/picture.component';

@NgModule({
  imports: [
    CommonModule,
//    ShowRoutingModule
  ],
  declarations: [ShowComponent, PictureComponent],
  exports: [ShowComponent]
})
export class ShowModule { }
